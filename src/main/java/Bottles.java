package assignment2;

class Bottles {

	public String verse(int verseNumber) {

		//bottles
		String s1=" bottles of beer on the wall, ";
		String s2=" bottles of beer.\n";
		String s3="Take one down and pass it around, ";
		String s4=" bottles of beer on the wall.\n";

		//bottle ==2
		String ss2=" bottle of beer on the wall.\n";

		//for bottle
		String s5=" bottle of beer on the wall, ";
		String s6=" bottle of beer.\n";
		String s7="Take it down and pass it around, no more bottles of beer on the wall.\n";

		if(verseNumber>2){
		return verseNumber+ s1 + verseNumber+ s2
	               	+ s3 + (verseNumber-1) + s4;
		}
		else if(verseNumber==2){
			return verseNumber+ s1 + verseNumber+ s2
					+ s3 + (verseNumber-1) + ss2;
		}
		else if(verseNumber==1){
			return verseNumber+ s5 + verseNumber+ s6
					+ s7;
		}
		else{
			return "No more bottles of beer on the wall, " + "no more bottles of beer.\n"
					+ "Go to the store and buy some more, " + "99 bottles of beer on the wall.\n";
		}

	}

	public String verse(int startVerseNumber, int endVerseNumber) {
		String res="";
		for(int i=startVerseNumber;i>=endVerseNumber;i--){
			if(i!=endVerseNumber){res=res+verse(i)+"\n";}
			else{res=res+verse(i);}
		}
		return res;
	}

	public String song() {
		return verse(99,0);
	}
}

